use anyhow::Context;
use log::debug;

enum DdotsRunnerError {
    InputError(anyhow::Error),
    SolutionTestingError(anyhow::Error),
    ReportingError(anyhow::Error),
}

impl DdotsRunnerError {
    fn exit_code(&self) -> i32 {
        match self {
            Self::InputError(_) => 1,
            Self::SolutionTestingError(_) => 2,
            Self::ReportingError(_) => 3,
        }
    }
}

fn run(mut config: ddots_runner::Config) -> Result<(), DdotsRunnerError> {
    env_logger::Builder::from_default_env()
        .format_timestamp_nanos()
        .init();

    // TODO: Remove this hack once DDOTS can pass `testing_mode` via environment variables on
    // each execution.
    let testing_mode_filepath = config.problem_root.join("testing_mode");
    if testing_mode_filepath.exists() {
        let testing_mode = fs_utils::read::head_to_string(&testing_mode_filepath, 100)
            .context("Testing mode could not be read.")
            .map_err(DdotsRunnerError::InputError)?;
        config.testing_mode = testing_mode.as_str().parse().map_err(|err| {
            DdotsRunnerError::InputError(anyhow::anyhow!("Failed to parse testing mode: {}", err))
        })?;
    }

    debug!("Config: {:#?}", config);

    let mut testing_context =
        ddots_runner::TestingContext::from_config(&config).map_err(DdotsRunnerError::InputError)?;

    let testing_report = ddots_runner::test_solution(&mut testing_context)
        .context("The solution could not be tested.")
        .map_err(DdotsRunnerError::SolutionTestingError)?;
    debug!("Testing Report: {:#?}", testing_report);

    let print = || {
        use ddots_runner::common::TestingReportOutputFormat;
        match config.report_output_format {
            TestingReportOutputFormat::Human => testing_report.print_in_human_format(),
            TestingReportOutputFormat::JSON => testing_report.print_in_json_format(),
            TestingReportOutputFormat::Legacy => testing_report.print_in_legacy_format(),
        }
    };
    print()
        .context("The testing report could not be printed.")
        .map_err(DdotsRunnerError::ReportingError)?;

    Ok(())
}

#[paw::main]
fn main(config: ddots_runner::Config) {
    match run(config) {
        Ok(_) => {}
        Err(err) => {
            match &err {
                DdotsRunnerError::InputError(inner_err) => println!("InputError: {:?}", inner_err),
                DdotsRunnerError::SolutionTestingError(inner_err) => {
                    println!("SolutionTestingError: {:?}", inner_err)
                }
                DdotsRunnerError::ReportingError(inner_err) => {
                    println!("ReportingError: {:?}", inner_err)
                }
            }
            std::process::exit(err.exit_code());
        }
    }
}
