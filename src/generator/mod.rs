use std::convert::TryFrom;
use std::path::PathBuf;

use anyhow::Context;
use log::{debug, info};
use nix::sys::signal::Signal;

use crate::extensions::process::{SandboxOptions, SandboxedCommand};

#[derive(Debug)]
pub struct Generator<'a> {
    sandbox_options: &'a SandboxOptions,
    sandbox_root: &'a PathBuf,
    generator_exe_filepath: &'a PathBuf,
    previous_solution_output_filepath: &'a PathBuf,
}

impl<'a> Generator<'a> {
    pub fn new(
        sandbox_options: &'a SandboxOptions,
        sandbox_root: &'a PathBuf,
        generator_exe_filepath: &'a PathBuf,
        previous_solution_output_filepath: &'a PathBuf,
    ) -> Self {
        Self {
            sandbox_options,
            sandbox_root,
            generator_exe_filepath,
            previous_solution_output_filepath,
        }
    }

    pub fn execute(&self) -> anyhow::Result<()> {
        let mut generator_process = std::process::Command::new_sandbox();
        let generator_process = generator_process
            .current_dir(&self.sandbox_root)
            .stdin(std::process::Stdio::null())
            .stdout(std::fs::File::create(self.sandbox_root.join("stdout.txt"))?);
        let generator_process = generator_process
            .spawn_in_sandbox(
                &[
                    self.generator_exe_filepath.as_ref(),
                    std::path::Path::new("./shared-context.dat"),
                    self.previous_solution_output_filepath.as_ref(),
                ],
                self.sandbox_options,
            )
            .with_context(|| {
                format!(
                    "The generator ({:?}) could not be executed.",
                    generator_process
                )
            })?;

        let generator_exit_code = generator_process.output.status.code();
        debug!(
            "The generator exited with {:?} exit code and produced the following stderr:\n{}",
            generator_exit_code,
            String::from_utf8_lossy(&generator_process.output.stderr)
        );

        match generator_exit_code {
            Some(0) => {
                info!("The generator finished successfully.");
            }
            Some(127) => {
                // "command not found" or missing shared libraries error
                anyhow::bail!(
                    "The generator could not be started. (stdout: \"{:?}\", stderr: \"{:?}\")",
                    generator_process.output.stdout,
                    generator_process.output.stderr
                );
            }
            Some(255) => {
                anyhow::bail!(
                    "The generator failed due to an internal error: {}",
                    String::from_utf8_lossy(&generator_process.output.stderr)
                );
            }
            Some(generator_exit_code) => {
                match Signal::try_from(generator_exit_code - 128) {
                    // Generator misbehaved and SIGKILL signal was sent to it due to ML or TL.
                    Ok(signal_name @ Signal::SIGKILL)
                    // Generator crashed due to a Runtime Error.
                    | Ok(signal_name @ Signal::SIGSEGV)
                    // Generator misbehaved and SIGVTALRM signal was sent to it due to TL.
                    | Ok(signal_name @ Signal::SIGVTALRM) => {
                        anyhow::bail!(
                            "The generator has been interrupted due to ML, TL, or \
                            RE reasons: {:?}",
                            signal_name
                        );
                    },
                    _ => {
                        anyhow::bail!(
                            "The generator exited with an unexpected exit code: {}",
                            generator_exit_code
                        );
                    },
                }
            }

            None => anyhow::bail!("The exit code of generator execution is not set."),
        }
        Ok(())
    }
}
