use std::borrow::Cow;
use std::convert::TryFrom;
use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::io::FromRawFd;
use std::path::Path;
use std::process::Command;

use anyhow::Context;
use log::{debug, error, info};
use nix::sys::signal::Signal;
use take_while_with_failure::TakeWhileWithFailureIterator;

use crate::checker::Checker;
use crate::common::{
    IOType, ProblemTestSpecification, SolutionTestStatus, TestReport, TestingMode, TestingReport,
    TestingReportExtraInfo,
};
use crate::extensions::process::{SandboxStats, SandboxedCommand};
use crate::generator::Generator;

use super::{TestingContext, TestingContextMode};

fn setup_pty_pipe() -> anyhow::Result<nix::pty::OpenptyResult> {
    use nix::sys::termios;

    let pty = nix::pty::openpty(None, None).context("PTY could not be initialized.")?;

    // Disable echoing child output to child input
    let mut slave_attrs =
        termios::tcgetattr(pty.slave).context("PTY could not be configured (tcgetattr).")?;
    termios::cfmakeraw(&mut slave_attrs);
    termios::tcsetattr(pty.slave, termios::SetArg::TCSANOW, &slave_attrs)
        .context("PTY could not be configured (tcsetattr).")?;

    Ok(pty)
}

fn setup_solution_execution(
    testing_context: &TestingContext,
) -> anyhow::Result<(
    impl FnOnce() -> anyhow::Result<SandboxStats>,
    Option<impl FnOnce() -> anyhow::Result<SandboxStats>>,
)> {
    let nix::pty::OpenptyResult {
        master: pty_master,
        slave: pty_slave,
    } = setup_pty_pipe()?;

    let solution_stdin = match testing_context.problem_specification.input_type {
        IOType::Stream => fs::File::open(&testing_context.get_current_input_filepath())
            .context("Current test input file cannot be opened.")?
            .into(),
        IOType::File => std::process::Stdio::null(),
        IOType::Pipe => unsafe { std::process::Stdio::from_raw_fd(pty_slave) },
    };
    let solution_stdout = match testing_context.problem_specification.output_type {
        IOType::Stream | IOType::File => {
            fs::File::create(&testing_context.get_current_stdout_filepath())
                .context("Current stdout file cannot be created.")?
                .into()
        }
        IOType::Pipe => unsafe { std::process::Stdio::from_raw_fd(pty_slave) },
    };

    let solution_sandbox_options = testing_context.solution_sandbox_options();
    let solution_sandbox_root = testing_context.solution_sandbox_root.clone();
    let solution_stderr_filepath = testing_context.get_current_stderr_filepath();
    let solution_cmd = testing_context
        .render_solution_execution_command()
        .context("Solution Execution command could not be rendered.")?;
    let execute_solution_fn = move || {
        let solution_stderr = fs::File::create(&solution_stderr_filepath)
            .context("Current stderr file cannot be opened.")?;

        Command::new_sandbox()
            .current_dir(&solution_sandbox_root)
            .stdin(solution_stdin)
            .stdout(solution_stdout)
            .stderr(solution_stderr)
            .spawn_in_sandbox(&solution_cmd, &solution_sandbox_options)
            .context("Solution process failed to execute")
    };

    // TODO: Decouple the interactor into a separate module.
    if let Some(ref interactor_exe_filepath) = testing_context.interactor_exe_filepath {
        let interactor_stdin = match testing_context.problem_specification.output_type {
            IOType::Stream | IOType::File => std::process::Stdio::null(),
            IOType::Pipe => unsafe { std::process::Stdio::from_raw_fd(pty_master) },
        };
        let interactor_stdout = match testing_context.problem_specification.input_type {
            IOType::Stream | IOType::File => std::process::Stdio::null(),
            IOType::Pipe => unsafe { std::process::Stdio::from_raw_fd(pty_master) },
        };

        let options = testing_context.interactor_sandbox_options();
        let interactor_exe_filepath = interactor_exe_filepath.clone();
        let interactor_sandbox_root = testing_context.semitrusted_service_sandbox_root.clone();

        let execute_interactor_fn = move || {
            Command::new_sandbox()
                .current_dir(&interactor_sandbox_root)
                .stdin(interactor_stdin)
                .stdout(interactor_stdout)
                .spawn_in_sandbox(&[interactor_exe_filepath], &options)
                .context("Solution process failed to execute")
        };

        Ok((execute_solution_fn, Some(execute_interactor_fn)))
    } else {
        Ok((execute_solution_fn, None))
    }
}

impl SolutionTestStatus {
    fn from_interactor_exit_code(exit_code: i32) -> Self {
        match exit_code {
            0 => {
                info!("The interactor reported OK.");
                SolutionTestStatus::OK
            }
            1 => {
                info!("The interactor reported WA.");
                SolutionTestStatus::WrongAnswer
            }
            2 => {
                info!("The interactor reported PE.");
                SolutionTestStatus::PresentationError
            }
            3 => {
                info!("The interactor reported failure.");
                SolutionTestStatus::CheckerCrash
            }
            127 => {
                info!("The interactor could not be started.");
                SolutionTestStatus::CheckerCrash
            }
            255 => SolutionTestStatus::UnexpectedError,
            exit_code => match Signal::try_from(exit_code - 128) {
                Ok(Signal::SIGPIPE) => {
                    info!("The interactor has lost pipe connection to the solution, which indicates runtime error of the solution");
                    SolutionTestStatus::PresentationError
                }
                Ok(signal_name) => {
                    info!(
                        "The interactor has been interrupted with {:?} signal",
                        signal_name
                    );
                    SolutionTestStatus::CheckerCrash
                }
                Err(_) => {
                    info!(
                        "The interactor exited with an unexpected exit code {}",
                        exit_code
                    );
                    SolutionTestStatus::CheckerCrash
                }
            },
        }
    }
}

fn execute_solution(
    testing_context: &TestingContext,
) -> anyhow::Result<(SandboxStats, TestReport)> {
    let (execute_solution_fn, execute_interactor_fn) = setup_solution_execution(&testing_context)?;
    let (solution_process_info, interactor_process_info) =
        if let Some(execute_interactor_fn) = execute_interactor_fn {
            let interactor_execution_thread = std::thread::spawn(execute_interactor_fn);
            let solution_execution_thread = std::thread::spawn(execute_solution_fn);
            (
                solution_execution_thread.join().map_err(|err| {
                    anyhow::anyhow!("Solution execution thread has crashed: {:?}", err)
                })??,
                Some(interactor_execution_thread.join().map_err(|err| {
                    anyhow::anyhow!("Interactor execution thread has crashed: {:?}", err)
                })??),
            )
        } else {
            (execute_solution_fn()?, None)
        };

    let mut test_report = TestReport::from_sandbox_stats(&solution_process_info);

    let solution_exit_code = solution_process_info.output.status.code().ok_or_else(|| {
        anyhow::anyhow!("The solution execution has been interrupted (no exit code)")
    })?;
    info!("The solution exited with exit code {}.", solution_exit_code);

    test_report.status = match Signal::try_from(solution_exit_code - 128) {
        Ok(Signal::SIGVTALRM) => {
            info!("The solution execution has been interrupted due to TL.");
            Some(SolutionTestStatus::TimeLimitExceeded)
        }

        Ok(Signal::SIGSEGV) => {
            info!("The solution has crashed (segmentation fault).");
            Some(SolutionTestStatus::RuntimeError)
        }

        Ok(Signal::SIGKILL) => {
            info!(
                "The solution execution has been interrupted due to ML (most likely), TL, or FF."
            );
            // It is either ML or TL, but we have to analyse the stats to tell which one.
            if test_report.execution_cpu_time > testing_context.get_cpu_time_limit() * 4 {
                if testing_context.config.ignore_blocked_sigvtalrm {
                    // Some interpreters (e.g. Ruby) may have SIGVTALRM signal
                    // reserved for internal use and we cannot find any other
                    // solution, but setitimer, which can send only SIGVTALRM
                    // signal based on a process CPU usage. Thus, we rely on
                    // real-time SIGKILL signal here.
                    Some(SolutionTestStatus::TimeLimitExceeded)
                } else {
                    test_report.add_extra_message(
                        "It seems that the solution is a fork-bomb or has ignored \
                         SIGVTALRM signal sent on CPU time limit exceeded."
                            .into(),
                    );
                    Some(SolutionTestStatus::ForbiddenFunctionDetected)
                }
            } else if test_report.memory_peak >= testing_context.problem_specification.memory_limit
            {
                // "Normal" excess of the memory limit happened.
                Some(SolutionTestStatus::MemoryLimitExceeded)
            } else if test_report.execution_cpu_time < testing_context.get_cpu_time_limit() {
                // This is a catch for `sleep()`-like solutions (e.g. hang on IO)
                if test_report.execution_real_time > testing_context.get_real_time_limit() {
                    test_report.add_extra_message(
                        "It seems that the solution hanged. It might be due to an undefined \
                         behavior (e.g. out-of-range access) or a blocking operation (e.g. \
                         sleep, blocked reading/writing). Thus, the Testing System will not \
                         proceed testing this solution."
                            .into(),
                    );
                    Some(SolutionTestStatus::ForbiddenFunctionDetected)
                } else {
                    Some(SolutionTestStatus::RuntimeError)
                }
            } else {
                // "Normal" excess of the time limit happened,
                // though the solution ignored SIGVTALRM signal.
                Some(SolutionTestStatus::TimeLimitExceeded)
            }
        }

        _ => {
            if test_report.execution_cpu_time > testing_context.get_cpu_time_limit() {
                Some(SolutionTestStatus::TimeLimitExceeded)
            } else {
                None
            }
        }
    };

    if let Ok(output) = fs_utils::read::head_to_string_with_message(
        &testing_context.get_current_stderr_filepath(),
        10240,
        "... (truncated output)",
    ) {
        let output = output.trim();
        if !output.is_empty() {
            debug!("The solution has produced the following log:\n{}", output);
        }
    }

    // TODO: Decouple the interactor into a separate module.
    if let Some(interactor_process_info) = interactor_process_info {
        if test_report.status.is_none() {
            let interactor_exit_code =
                interactor_process_info
                    .output
                    .status
                    .code()
                    .ok_or_else(|| {
                        anyhow::anyhow!(
                            "The interactor execution has been interrupted (no exit code)"
                        )
                    })?;
            info!(
                "The interactor exited with exit code {}.",
                interactor_exit_code
            );
            let output = String::from_utf8_lossy(&interactor_process_info.output.stderr);
            let output = output.trim();
            if !output.is_empty() {
                debug!("The interactor has produced the following log:\n{}", output);
                test_report.add_extra_message(output.to_owned().into());
            }

            test_report.status = Some(if interactor_exit_code != 0 && solution_exit_code != 0 {
                info!(
                    "The interactor did not report OK and solution exited with a non-zero \
                     exit code, so it seems to be Runtime Error."
                );
                SolutionTestStatus::RuntimeError
            } else {
                let status = SolutionTestStatus::from_interactor_exit_code(interactor_exit_code);
                if let SolutionTestStatus::UnexpectedError = status {
                    error!("The interactor failed due to an internal error: {}", output);
                }
                status
            });
        }
    }

    Ok((solution_process_info, test_report))
}

fn prepare_sandbox_folder<P: AsRef<Path>>(sandbox: P) -> anyhow::Result<()> {
    fs_utils::remove::cleanup_folder(sandbox.as_ref())
        .context("The solution sandbox could not be cleaned up.")?;
    match fs_utils::check::is_folder_empty(sandbox.as_ref()) {
        Ok(true) => Ok(()),
        Ok(false) => anyhow::bail!(
            "The solution sandbox is not empty after cleanup, it is unsafe to proceed"
        ),
        Err(why) => anyhow::bail!(
            "The solution sandbox could not be checked if it is empty, \
             it is unsafe to proceed: {}",
            why
        ),
    }
}

fn test_solution_on_a_single_test(
    test_specification: &ProblemTestSpecification,
    testing_context: &TestingContext,
) -> anyhow::Result<TestReport> {
    info!(
        "The solution is going to be tested on {:?}",
        test_specification
    );

    prepare_sandbox_folder(&testing_context.solution_sandbox_root)?;

    fs::copy(
        &testing_context.config.solution_filepath,
        &testing_context.solution_exe_filepath,
    )
    .context("The solution executable could not be copied to the sandbox.")?;
    fs::set_permissions(
        &testing_context.solution_exe_filepath,
        fs::Permissions::from_mode(0o777),
    )
    .context("The permissions for the solution executable in the sandbox could not be set.")?;

    let test_input_filepath =
        if let Some(ref generator_exe_filepath) = testing_context.generator_exe_filepath {
            if let TestingContextMode::InitialRun { .. } = testing_context.mode.get() {
                Generator::new(
                    &testing_context.generator_sandbox_options(),
                    &testing_context.semitrusted_service_sandbox_root,
                    generator_exe_filepath,
                    &testing_context.previous_solution_output_filepath,
                )
                .execute()
                .context("The generator could not succeed.")?;
            }
            Cow::Borrowed(&testing_context.generator_output_filepath)
        } else {
            Cow::Owned(
                testing_context
                    .config
                    .problem_root
                    .join(&test_specification.input_filepath),
            )
        };
    let current_test_input_filepath = testing_context.get_current_input_filepath();
    fs::copy(test_input_filepath.as_ref(), &current_test_input_filepath)
        .context("The input file could not be copied to the sandbox.")?;

    fs::set_permissions(
        &current_test_input_filepath,
        fs::Permissions::from_mode(0o666),
    )
    .context("The permissions for the input file in the sandbox could not be set.")?;

    let test_answer_filepath = testing_context
        .config
        .problem_root
        .join(&test_specification.answer_filepath);
    if testing_context.interactor_exe_filepath.is_some() {
        let current_test_answer_filepath = testing_context
            .semitrusted_service_sandbox_root
            .join("answer.txt");
        fs::copy(&test_answer_filepath, &current_test_answer_filepath)
            .context("The answer file could not be copied to the semitrusted sandbox.")?;

        fs::set_permissions(
            &current_test_answer_filepath,
            fs::Permissions::from_mode(0o666),
        )
        .context(
            "The permissions for the answer file in the semitrusted sandbox could not be set.",
        )?;
    }

    let (solution_process_info, mut test_report) = execute_solution(testing_context)?;

    if test_report.status.is_some() {
        return Ok(test_report);
    }

    let solution_output_filepath = testing_context
        .solution_sandbox_root
        .join(&testing_context.problem_specification.output_filename);
    if let IOType::Stream = testing_context.problem_specification.output_type {
        if !solution_output_filepath.exists() {
            let solution_stdout_filepath = testing_context.get_current_stdout_filepath();
            if fs::metadata(&solution_stdout_filepath)
                .map(|m| m.len() > 0)
                .unwrap_or(false)
            {
                fs::rename(solution_stdout_filepath, &solution_output_filepath)
                    .context("The solution stdout file could not be renamed for checker.")?;
            }
        }
    };

    // Checker should be able to read the solution output, thus we set 0o644 permissions,
    // but if the file does not exist in the first place, let checker do its work.
    if let Err(error) =
        fs::set_permissions(&solution_output_filepath, fs::Permissions::from_mode(0o644))
            .context("Solution output file could not be chmod'ed")
    {
        if solution_output_filepath.exists() {
            return Err(error);
        }
    }

    let checker_exe_filepath = testing_context
        .checker_exe_filepath
        .as_ref()
        .ok_or_else(|| anyhow::anyhow!("Checker is not defined in the problem."))?;
    let checker_report = Checker::new(
        &testing_context.checker_sandbox_options(),
        &testing_context.semitrusted_service_sandbox_root,
        &checker_exe_filepath,
        &test_input_filepath,
        &test_answer_filepath,
        &solution_output_filepath,
        solution_process_info.output.status.code(),
    )
    .execute()
    .context("The checker failure occured.")?;
    test_report.status = Some(checker_report.status);

    match test_report.status {
        Some(SolutionTestStatus::OK) => {
            test_report.set_scored_points(test_specification.points);
        }
        Some(SolutionTestStatus::TimeLimitExceeded)
        | Some(SolutionTestStatus::MemoryLimitExceeded) => {}
        _ => {
            if let Ok(output) = fs_utils::read::head_to_string_with_message(
                &solution_output_filepath,
                10240,
                "... (truncated output)",
            ) {
                if !output.is_empty() {
                    debug!(
                        "The solution has produced the following output:\n{}",
                        output
                    );
                    test_report.set_extra_output(output);
                }
            }

            if !checker_report.message.is_empty() {
                test_report.add_extra_message(checker_report.message.into());
            }
        }
    };

    if testing_context.generator_exe_filepath.is_some() {
        fs::remove_file(&testing_context.previous_solution_output_filepath).ok();
        if test_report.is_ok() && solution_output_filepath.exists() {
            fs::rename(
                &solution_output_filepath,
                &testing_context.previous_solution_output_filepath,
            )
            .context(
                "The solution output file could not be renamed for generator next iteration.",
            )?;
        }
    }

    Ok(test_report)
}

pub fn test_solution(testing_context: &mut TestingContext) -> anyhow::Result<TestingReport> {
    let testing_mode = testing_context.config.testing_mode;
    testing_context.mode.set(TestingContextMode::InitialRun {
        rerun_attempts_remain: testing_context.config.max_rerun_attempts,
    });
    let tests = testing_context
        .problem_specification
        .tests
        .iter()
        .map(|test_specification| {
            // TODO: implement `testing_context.reruner` iterator to clean
            // things up and hide the `mode` implementation detail.
            let mut rerun_attempts_remain = if let TestingContextMode::InitialRun {
                rerun_attempts_remain,
            } = testing_context.mode.get()
            {
                rerun_attempts_remain
            } else {
                0
            };
            let test_result = loop {
                if let TestingContextMode::Rerunning = testing_context.mode.get() {
                    rerun_attempts_remain -= 1;
                }

                let test_result =
                    test_solution_on_a_single_test(test_specification, &testing_context);
                if let Ok(TestReport {
                    status: Some(SolutionTestStatus::TimeLimitExceeded),
                    ..
                }) = test_result
                {
                    if rerun_attempts_remain > 1 {
                        testing_context.mode.set(TestingContextMode::Rerunning);
                        continue;
                    }
                }
                break test_result;
            };
            if rerun_attempts_remain >= 1 {
                rerun_attempts_remain = testing_context.config.max_rerun_attempts;
            }
            testing_context.mode.set(TestingContextMode::InitialRun {
                rerun_attempts_remain,
            });
            test_result
        })
        .take_while_with_failure(|test_result| {
            test_result
                .as_ref()
                .map(|test_report| !test_report.is_breaking_error())
                .unwrap_or(false)
        });

    let tests = match testing_mode {
        TestingMode::Full => tests.collect::<Result<_, _>>(),
        TestingMode::FirstFail => tests
            .take_while_with_failure(|test_result| {
                test_result.as_ref().map(TestReport::is_ok).unwrap_or(false)
            })
            .collect(),
        TestingMode::One => tests.take(1).collect(),
    }?;

    Ok(TestingReport {
        extra_info: TestingReportExtraInfo {
            time_factor: testing_context.config.time_factor,
            solution_start_time_compensation: testing_context.solution_start_time_compensation,
        },
        tests,
    })
}
