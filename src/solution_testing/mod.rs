use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::path::PathBuf;
use std::process::Command;
use std::time;

use anyhow::Context;
use log::warn;
use maplit::hashmap;
use strfmt::strfmt;

use crate::common::{Config, IOType, ProblemSpecification};
use crate::extensions::memory_size::MemorySize;
use crate::extensions::process::{SandboxOptions, SandboxStdioMode, SandboxedCommand};

mod impls;

pub use self::impls::test_solution;

#[derive(Debug, Copy, Clone)]
enum TestingContextMode {
    InitialRun { rerun_attempts_remain: u8 },
    Rerunning,
}

impl Default for TestingContextMode {
    fn default() -> Self {
        TestingContextMode::InitialRun {
            rerun_attempts_remain: 0,
        }
    }
}

#[derive(Debug)]
pub struct TestingContext<'a> {
    solution_exe_filepath: PathBuf,
    generator_exe_filepath: Option<PathBuf>,
    interactor_exe_filepath: Option<PathBuf>,
    checker_exe_filepath: Option<PathBuf>,
    solution_sandbox_root: PathBuf,
    semitrusted_service_sandbox_root: PathBuf,
    generator_output_filepath: PathBuf,
    previous_solution_output_filepath: PathBuf,
    solution_start_time_compensation: time::Duration,
    mode: std::cell::Cell<TestingContextMode>,
    config: &'a Config,
    problem_specification: ProblemSpecification,
}

impl<'a> TestingContext<'a> {
    pub fn from_config(config: &'a Config) -> anyhow::Result<Self> {
        let problem_xml_filepath = config.problem_root.join("Problem.xml");
        anyhow::ensure!(
            problem_xml_filepath.exists(),
            "DDOTS_RUNNER_PROBLEM_ROOT ({:?}) must contain Problem.xml file",
            config.problem_root
        );

        let problem_specification = ProblemSpecification::from_readable(
            &fs::File::open(&problem_xml_filepath).context("Failed to open Problem.xml")?,
        )
        .map_err(|why| anyhow::anyhow!("Problem.xml parsing failed due to {}", why))?;

        let semitrusted_service_sandbox_root = config.sandbox_root.join("semitrusted-services");
        let solution_sandbox_root = config.sandbox_root.join("solution");

        let mut context = Self {
            solution_exe_filepath: solution_sandbox_root.join(&config.sandbox_solution_binary_name),
            generator_exe_filepath: problem_specification
                .generator_exe_filepath
                .as_ref()
                .map(|ref filepath| config.problem_root.join(filepath)),
            interactor_exe_filepath: problem_specification
                .interactor_exe_filepath
                .as_ref()
                .map(|ref filepath| config.problem_root.join(filepath)),
            checker_exe_filepath: problem_specification
                .checker_exe_filepath
                .as_ref()
                .map(|ref filepath| config.problem_root.join(filepath)),
            generator_output_filepath: semitrusted_service_sandbox_root.join("stdout.txt"),
            previous_solution_output_filepath: semitrusted_service_sandbox_root
                .join("previous-solution-output.txt"),
            solution_sandbox_root,
            semitrusted_service_sandbox_root,
            solution_start_time_compensation: time::Duration::default(),
            mode: Default::default(),
            config,
            problem_specification,
        };
        context.init()?;
        Ok(context)
    }

    pub fn init(&mut self) -> anyhow::Result<()> {
        if !self.config.sandbox_root.exists() {
            fs::create_dir(&self.config.sandbox_root)
                .context("Sandbox folder could not be created")?;
        }
        fs::set_permissions(&self.config.sandbox_root, fs::Permissions::from_mode(0o755))
            .context("Sandbox folder for semitrusted services could not be chmod'ed")?;

        if !self.semitrusted_service_sandbox_root.exists() {
            fs::create_dir(&self.semitrusted_service_sandbox_root)
                .context("Sandbox folder for semitrusted services could not be created")?;
        }
        fs::set_permissions(
            &self.semitrusted_service_sandbox_root,
            fs::Permissions::from_mode(0o700),
        )
        .context("Sandbox folder for semitrusted services could not be chmod'ed")?;
        nix::unistd::chown(
            &self.semitrusted_service_sandbox_root,
            Some(nix::unistd::Uid::from_raw(
                self.config.semitrusted_service_sandbox_user.uid(),
            )),
            Some(nix::unistd::Gid::from_raw(
                self.config
                    .semitrusted_service_sandbox_user
                    .primary_group_id(),
            )),
        )
        .context("Sandbox folder for semitrusted services could not be chown'ed")?;

        if !self.solution_sandbox_root.exists() {
            fs::create_dir(&self.solution_sandbox_root)
                .context("Sandbox folder for Solution could not be created")?;
        }
        fs::set_permissions(
            &self.solution_sandbox_root,
            fs::Permissions::from_mode(0o777),
        )
        .context("Sandbox folder for Solution could not be chmod'ed")?;

        if let Some(ref minimal_program_filepath) = self.config.minimal_program_filepath {
            let mut sandbox_options = SandboxOptions::default();
            sandbox_options.cgroup_namespace = self.config.sandbox_cgroup_namespace.clone();
            sandbox_options.user_memory_limit = MemorySize::from_mebibytes(64);
            let mut minimal_program_execution_times: Vec<_> = (0..5)
                .map(|_| {
                    Ok(Command::new_sandbox()
                        .current_dir(&self.solution_sandbox_root)
                        .spawn_in_sandbox(
                            &self
                                .render_execution_command(minimal_program_filepath)
                                .context(
                                    "Minimal program execution command could not be rendered.",
                                )?,
                            &sandbox_options,
                        )
                        .context("Minimal program could not be executed.")?
                        .execution_cpu_time)
                })
                .collect::<Result<_, anyhow::Error>>()?;
            minimal_program_execution_times.sort_unstable();
            self.solution_start_time_compensation = minimal_program_execution_times[2];
        }

        Ok(())
    }

    fn get_current_input_filepath(&self) -> PathBuf {
        self.solution_sandbox_root
            .join(match self.problem_specification.input_type {
                IOType::Stream => "input.txt",
                IOType::File | IOType::Pipe => &self.problem_specification.input_filename,
            })
    }

    fn get_current_stdout_filepath(&self) -> PathBuf {
        self.solution_sandbox_root.join("stdout.txt")
    }

    fn get_current_stderr_filepath(&self) -> PathBuf {
        self.solution_sandbox_root.join("stderr.txt")
    }

    #[inline]
    fn get_cpu_time_limit(&self) -> time::Duration {
        (self.problem_specification.time_limit * ((self.config.time_factor * 100.0) as u32)) / 100
            + self.solution_start_time_compensation
    }

    #[inline]
    fn get_real_time_limit(&self) -> time::Duration {
        let factor = if self.config.ignore_blocked_sigvtalrm {
            1.25
        } else {
            self.config.real_time_limit_factor
        };
        self.get_cpu_time_limit().mul_f64(factor.into())
    }

    #[inline]
    fn get_memory_limit(&self) -> MemorySize {
        self.problem_specification.memory_limit * self.config.memory_factor
    }

    fn render_execution_command(
        &self,
        execution_filepath: &PathBuf,
    ) -> anyhow::Result<Vec<String>> {
        let memory_limit_bytes = self.problem_specification.memory_limit.to_bytes();
        let memory_limit_bytes_str = memory_limit_bytes.to_string();
        let execution_command_context: std::collections::HashMap<String, _> = hashmap! {
            "execution_filepath".into() => execution_filepath.to_str()
                .ok_or_else(|| {
                    anyhow::anyhow!(
                        "execution filepath '{:?}' is not a valid unicode.",
                        execution_filepath,
                    )
                })?,
            "memory_limit_bytes".into() => &memory_limit_bytes_str,
        };
        let args = strfmt(
            &self.config.execution_command_template,
            &execution_command_context,
        )
        .map_err(|why| anyhow::anyhow!("Execution command could not be rendered due to {}", why))?;
        serde_json::from_str(&args).map_err(|why| {
            anyhow::anyhow!("Execution command could not be rendered due to {}", why)
        })
    }

    #[inline]
    fn render_solution_execution_command(&self) -> anyhow::Result<Vec<String>> {
        self.render_execution_command(&self.solution_exe_filepath)
    }

    fn solution_sandbox_options(&self) -> SandboxOptions {
        let mut sandbox_options = SandboxOptions::default();
        sandbox_options.cgroup_namespace = self.config.sandbox_cgroup_namespace.clone();
        sandbox_options.real_time_limit = self.get_real_time_limit();
        sandbox_options.cpu_time_limit = self.get_cpu_time_limit() + time::Duration::from_millis(5);
        sandbox_options.user_memory_limit = self.get_memory_limit();
        sandbox_options.user_id =
            nix::unistd::Uid::from_raw(self.config.solution_sandbox_user.uid());
        sandbox_options.group_id =
            nix::unistd::Gid::from_raw(self.config.solution_sandbox_user.primary_group_id());
        sandbox_options.stdio = SandboxStdioMode::ErrorsRedirect;
        sandbox_options
    }

    /// These sandbox limits are used for processes such as generators and checkers.
    fn semitrusted_service_sandbox_options(&self) -> SandboxOptions {
        let mut sandbox_options = SandboxOptions::default();
        sandbox_options.cgroup_namespace = self.config.sandbox_cgroup_namespace.clone();
        sandbox_options.real_time_limit = self.get_real_time_limit() * 100;
        sandbox_options.cpu_time_limit = self.get_cpu_time_limit() * 100;
        sandbox_options.user_memory_limit = self.get_memory_limit() * 2.0;
        sandbox_options.user_id =
            nix::unistd::Uid::from_raw(self.config.semitrusted_service_sandbox_user.uid());
        sandbox_options.group_id = nix::unistd::Gid::from_raw(
            self.config
                .semitrusted_service_sandbox_user
                .primary_group_id(),
        );
        sandbox_options
    }

    #[inline]
    fn checker_sandbox_options(&self) -> SandboxOptions {
        self.semitrusted_service_sandbox_options()
    }

    #[inline]
    fn generator_sandbox_options(&self) -> SandboxOptions {
        self.semitrusted_service_sandbox_options()
    }

    #[inline]
    fn interactor_sandbox_options(&self) -> SandboxOptions {
        let mut sandbox_options = self.semitrusted_service_sandbox_options();
        sandbox_options.stdio = SandboxStdioMode::ErrorsRedirect;
        sandbox_options
    }
}

impl Drop for TestingContext<'_> {
    fn drop(&mut self) {
        if let Err(why) = fs_utils::remove::cleanup_folder(&self.config.sandbox_root) {
            warn!("Sandbox could not be cleaned up: {}", why);
        } else {
            match fs_utils::check::is_folder_empty(&self.config.sandbox_root) {
                Ok(true) => {}
                Ok(false) => warn!("Sandbox was cleaned, but still has some files."),
                Err(why) => warn!("Sandbox could not be checked if it is empty: {}", why),
            }
        }
    }
}
