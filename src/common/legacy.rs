use anyhow::anyhow;
use decimal::d128;

use crate::common::{SolutionTestStatus, TestingReport};

pub fn print_legacy_testing_report(testing_report: &TestingReport) -> anyhow::Result<()> {
    let mut messages = String::new();
    for (index, test) in testing_report.tests.iter().enumerate() {
        let mut scored_points = d128!(0);
        if let Some(ref extra) = test.extra {
            if let Some(_scored_points) = extra.scored_points {
                scored_points = _scored_points;
            }
            if !test.is_ok() {
                if let Some(ref message) = extra.message {
                    messages.push_str(message);
                    messages.push_str("\n");
                }
            };
        }

        println!(
            "{} {} {} {:.3} {}",
            index + 1,
            serde_json::to_value(test.status.unwrap_or(SolutionTestStatus::UnexpectedError))
                .map_err(|why| anyhow!(
                    "Testing status could not be serialized into a string due to {}",
                    why
                ))?
                .as_str()
                .ok_or_else(|| anyhow!("Testing status could not be displayed."))?,
            scored_points,
            testing_report
                .normalize_execution_cpu_time(test.execution_cpu_time)
                .as_millis(),
            test.memory_peak.to_bytes()
        );
    }
    if !messages.is_empty() {
        print!("\n{}", messages);
    }
    Ok(())
}
