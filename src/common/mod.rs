use std::fmt;
use std::io;
use std::path::PathBuf;
use std::time;

use serde_derive::{Deserialize, Serialize};
use structopt::StructOpt;

use crate::extensions::memory_size::MemorySize;
use crate::extensions::process::SandboxStats;
use crate::extensions::time::DurationExt;

mod legacy;

#[derive(Debug, Clone, Copy)]
pub enum TestingMode {
    One,
    FirstFail,
    Full,
}

impl Default for TestingMode {
    fn default() -> TestingMode {
        TestingMode::Full
    }
}

impl std::str::FromStr for TestingMode {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            _ if s.eq_ignore_ascii_case("full") => TestingMode::Full,
            _ if s.eq_ignore_ascii_case("first-fail") => TestingMode::FirstFail,
            _ if s.eq_ignore_ascii_case("one") => TestingMode::One,
            _ => return Err(format!("valid values: {}", Self::variants().join(", "))),
        })
    }
}

impl TestingMode {
    fn variants() -> &'static [&'static str] {
        &["one", "first-fail", "full"]
    }
}

#[derive(Debug, Clone, Copy)]
pub enum TestingReportOutputFormat {
    Human,
    JSON,
    Legacy,
}

impl std::str::FromStr for TestingReportOutputFormat {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            _ if s.eq_ignore_ascii_case("human") => TestingReportOutputFormat::Human,
            _ if s.eq_ignore_ascii_case("json") => TestingReportOutputFormat::JSON,
            _ if s.eq_ignore_ascii_case("legacy") => TestingReportOutputFormat::Legacy,
            _ => return Err(format!("valid values: {}", Self::variants().join(", "))),
        })
    }
}

impl TestingReportOutputFormat {
    fn variants() -> &'static [&'static str] {
        &["human", "json", "legacy"]
    }
}

fn parse_existing_path(s: &str) -> Result<PathBuf, io::Error> {
    let path = PathBuf::from(s);
    if !path.exists() {
        Err(io::Error::new(
            io::ErrorKind::NotFound,
            format!("The path does not exist: {:?}", path),
        ))
    } else {
        std::fs::canonicalize(path)
    }
}

fn parse_positive_f32(s: &str) -> anyhow::Result<f32> {
    let x = s.parse()?;
    if x <= 0.0 {
        anyhow::bail!("The value must be positive: {}", x);
    } else {
        Ok(x)
    }
}

fn parse_user(s: &str) -> anyhow::Result<users::User> {
    users::get_user_by_name(s).ok_or_else(|| anyhow::anyhow!("User '{}' not found.", s))
}

#[derive(Debug, Clone, StructOpt)]
#[structopt(
    setting(structopt::clap::AppSettings::ColoredHelp),
    setting(structopt::clap::AppSettings::NextLineHelp)
)]
pub struct Config {
    #[structopt(
        long,
        env = "DDOTS_RUNNER_SOLUTION_FILEPATH",
        parse(try_from_str = parse_existing_path)
    )]
    pub solution_filepath: PathBuf,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_PROBLEM_ROOT",
        parse(try_from_str = parse_existing_path)
    )]
    pub problem_root: PathBuf,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_SANDBOX_ROOT",
        default_value = "/tmp/ddots-runner-sandbox"
    )]
    pub sandbox_root: PathBuf,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_SANDBOX_SOLUTION_BINARY_NAME",
        default_value = "solution.bin"
    )]
    pub sandbox_solution_binary_name: PathBuf,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_TESTING_MODE",
        default_value = "full",
        possible_values(&TestingMode::variants()),
        case_insensitive(true)
    )]
    pub testing_mode: TestingMode,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_TIME_FACTOR",
        default_value = "1.0",
        parse(try_from_str = parse_positive_f32)
    )]
    pub time_factor: f32,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_MEMORY_FACTOR",
        default_value = "1.0",
        parse(try_from_str = parse_positive_f32)
    )]
    pub memory_factor: f32,

    // TODO: Implement a custom JSON-Jinja2-String type or redesign this from scratch.
    #[structopt(
        long,
        env = "DDOTS_RUNNER_EXECUTION_COMMAND_TEMPLATE",
        default_value = "[\"{execution_filepath}\"]"
    )]
    pub execution_command_template: String,
    #[structopt(long, env = "DDOTS_RUNNER_IGNORE_BLOCKED_SIGVTALRM")]
    pub ignore_blocked_sigvtalrm: bool,
    #[structopt(long, env = "DDOTS_RUNNER_MAX_RERUN_ATTEMPTS", default_value = "3")]
    pub max_rerun_attempts: u8,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_REAL_TIME_LIMIT_FACTOR",
        default_value = "10.0",
        parse(try_from_str = parse_positive_f32)
    )]
    pub real_time_limit_factor: f32,
    #[structopt(long, env = "DDOTS_RUNNER_MINIMAL_PROGRAM_FILEPATH")]
    pub minimal_program_filepath: Option<PathBuf>,

    #[structopt(
        long,
        env = "DDOTS_RUNNER_SANDBOX_CGROUP_NAMESPACE",
        default_value = ""
    )]
    pub sandbox_cgroup_namespace: PathBuf,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_SEMITRUSTED_SERVICE_SANDBOX_USER",
        default_value = "daemon",
        parse(try_from_str = parse_user)
    )]
    pub semitrusted_service_sandbox_user: users::User,
    #[structopt(
        long,
        env = "DDOTS_RUNNER_SOLUTION_SANDBOX_USER",
        default_value = "nobody",
        parse(try_from_str = parse_user)
    )]
    pub solution_sandbox_user: users::User,

    #[structopt(
        long,
        env = "DDOTS_RUNNER_REPORT_OUTPUT_FORMAT",
        default_value = "human",
        possible_values(&TestingReportOutputFormat::variants()),
        case_insensitive(true)
    )]
    pub report_output_format: TestingReportOutputFormat,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ProblemTestSpecification {
    #[serde(rename = "Input")]
    pub input_filepath: PathBuf,
    #[serde(rename = "Answer")]
    pub answer_filepath: PathBuf,
    #[serde(rename = "Points")]
    pub points: decimal::d128,
}

#[derive(Debug, Clone, Copy)]
pub enum IOType {
    File,
    Stream,
    Pipe,
}

impl Default for IOType {
    fn default() -> IOType {
        IOType::File
    }
}

#[derive(Debug, Default, Clone, Deserialize)]
pub struct ProblemSpecification {
    #[serde(
        rename = "TimeLimit",
        deserialize_with = "time::Duration::deserialize_from_seconds"
    )]
    pub time_limit: time::Duration,
    #[serde(
        rename = "MemoryLimit",
        deserialize_with = "MemorySize::deserialize_from_mebibytes"
    )]
    pub memory_limit: MemorySize,
    #[serde(rename = "InputFile")]
    pub input_filename: String,
    #[serde(skip_deserializing)]
    pub input_type: IOType,
    #[serde(rename = "OutputFile")]
    pub output_filename: String,
    #[serde(skip_deserializing)]
    pub output_type: IOType,
    #[serde(rename = "PatcherExe")]
    pub patcher_exe_filepath: Option<PathBuf>,
    #[serde(rename = "GeneratorExe")]
    pub generator_exe_filepath: Option<PathBuf>,
    #[serde(rename = "InteractorExe")]
    pub interactor_exe_filepath: Option<PathBuf>,
    #[serde(rename = "CheckerExe")]
    pub checker_exe_filepath: Option<PathBuf>,
    #[serde(rename = "PointsOnGold")]
    pub bonus_points_on_gold: Option<decimal::d128>,
    #[serde(rename = "Test")]
    pub tests: Vec<ProblemTestSpecification>,
}

impl ProblemSpecification {
    pub fn from_xml_str(xml_str: &str) -> Result<Self, serde_xml_rs::Error> {
        let mut problem_specification: Self = serde_xml_rs::from_str(xml_str)?;
        problem_specification.update_computed_values();
        Ok(problem_specification)
    }

    pub fn from_readable<R: io::Read>(xml_file: R) -> Result<Self, serde_xml_rs::Error> {
        let mut problem_specification: Self = serde_xml_rs::from_reader(xml_file)?;
        problem_specification.update_computed_values();
        Ok(problem_specification)
    }

    pub fn update_computed_values(&mut self) {
        self.input_type = match self.input_filename.as_ref() {
            "stdin" => IOType::Stream,
            "pipe" => IOType::Pipe,
            _ => IOType::File,
        };
        if let IOType::Stream | IOType::Pipe = self.input_type {
            self.input_filename = "input.txt".into();
        }
        self.output_type = match self.output_filename.as_ref() {
            "stdout" => IOType::Stream,
            "pipe" => IOType::Pipe,
            _ => IOType::File,
        };
        if let IOType::Stream | IOType::Pipe = self.output_type {
            self.output_filename = "output.txt".into();
        }
        if self.input_filename == self.output_filename {
            panic!("Problem.xml has the same values for InputFile and OutputFile, which will always lead to OK solutions! Fix it!");
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize)]
pub enum SolutionTestStatus {
    OK,
    #[serde(rename = "PE")]
    PresentationError,
    #[serde(rename = "WA")]
    WrongAnswer,
    #[serde(rename = "RE")]
    RuntimeError,
    #[serde(rename = "TL")]
    TimeLimitExceeded,
    #[serde(rename = "ML")]
    MemoryLimitExceeded,
    #[serde(rename = "FF")]
    ForbiddenFunctionDetected,
    #[serde(rename = "CC")]
    CheckerCrash,
    #[serde(rename = "UE")]
    UnexpectedError,
}

#[derive(Debug, Default, Clone, Serialize)]
pub struct TestReportExtraSection {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub scored_points: Option<decimal::d128>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub message: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub output: Option<String>,
}

#[derive(Debug, Clone, Serialize)]
pub struct TestReport {
    pub status: Option<SolutionTestStatus>,
    #[serde(serialize_with = "time::Duration::serialize_to_seconds")]
    pub execution_cpu_time: time::Duration,
    #[serde(serialize_with = "time::Duration::serialize_to_seconds")]
    pub execution_real_time: time::Duration,
    #[serde(serialize_with = "MemorySize::serialize_to_bytes")]
    pub memory_peak: MemorySize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extra: Option<TestReportExtraSection>,
}

impl Default for TestReport {
    fn default() -> Self {
        TestReport {
            status: None,
            execution_cpu_time: time::Duration::new(0, 0),
            execution_real_time: time::Duration::new(0, 0),
            memory_peak: MemorySize::from_bytes(0),
            extra: None,
        }
    }
}

impl TestReport {
    pub fn with_unexpected_error(message: std::borrow::Cow<'static, str>) -> Self {
        let mut test_report = Self::default();
        test_report.status = Some(SolutionTestStatus::UnexpectedError);
        test_report.add_extra_message(message);
        test_report
    }

    pub fn from_sandbox_stats(sandbox_stats: &SandboxStats) -> Self {
        Self {
            status: None,
            execution_cpu_time: sandbox_stats.execution_cpu_time,
            execution_real_time: sandbox_stats.execution_real_time,
            memory_peak: sandbox_stats.memory_peak,
            extra: None,
        }
    }

    pub fn set_scored_points(&mut self, scored_points: decimal::d128) {
        let extra = self
            .extra
            .get_or_insert_with(TestReportExtraSection::default);
        extra.scored_points = Some(scored_points);
    }

    pub fn set_extra_output<S: Into<String>>(&mut self, output: S) {
        let extra = self
            .extra
            .get_or_insert_with(TestReportExtraSection::default);
        extra.output = Some(output.into());
    }

    pub fn add_extra_message(&mut self, message: std::borrow::Cow<'static, str>) {
        if let Some(ref mut extra) = self.extra {
            if let Some(ref mut existing_message) = extra.message {
                existing_message.push_str("\n\n");
                existing_message.push_str(&message);
            } else {
                extra.message = Some(message.into_owned());
            }
        } else {
            let mut extra = TestReportExtraSection::default();
            extra.message = Some(message.into_owned());
            self.extra = Some(extra);
        };
    }

    pub fn is_ok(&self) -> bool {
        match self.status {
            Some(SolutionTestStatus::OK) => true,
            _ => false,
        }
    }

    pub fn is_breaking_error(&self) -> bool {
        match self.status {
            Some(SolutionTestStatus::ForbiddenFunctionDetected)
            | Some(SolutionTestStatus::CheckerCrash)
            | Some(SolutionTestStatus::UnexpectedError)
            | None => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize)]
pub struct TestingReportExtraInfo {
    pub time_factor: f32,
    pub solution_start_time_compensation: time::Duration,
}

impl fmt::Display for TestingReportExtraInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Extra info:\n  Global time factor: {:.2}\n  Solution start time compensation: {:.3}s",
            self.time_factor,
            self.solution_start_time_compensation.as_millis() as f64 / 1000.
        )
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct TestingReport {
    pub extra_info: TestingReportExtraInfo,
    pub tests: Vec<TestReport>,
}

impl fmt::Display for TestingReport {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}", self.extra_info)?;
        writeln!(f)?;
        writeln!(f, "Tests:")?;
        for (test_index, test) in self.tests.iter().enumerate() {
            writeln!(f, "Test #{:>03} ({:?}) took {:.3}s normalized CPU time / {:.3}s true CPU time / {:.3}s real time and peaked at {:.3} MiB.", test_index + 1, test.status.unwrap_or(SolutionTestStatus::UnexpectedError), self.normalize_execution_cpu_time(test.execution_cpu_time).as_millis() as f64 / 1000., test.execution_cpu_time.as_millis() as f64 / 1000., test.execution_real_time.as_millis() as f64 / 1000., test.memory_peak.to_kibibytes() as f64 / 1000.)?;
            if let Some(ref extra) = test.extra {
                if let Some(scored_points) = extra.scored_points {
                    writeln!(f, "  Scored points: {}", scored_points)?;
                }
                if let Some(ref message) = extra.message {
                    writeln!(
                        f,
                        "  Internal messages:\n===== BEGIN =====\n{}\n====== END ======",
                        message.trim_end()
                    )?;
                }
                if let Some(ref output) = extra.output {
                    writeln!(
                        f,
                        "  Solution output:\n===== BEGIN =====\n{}\n====== END ======",
                        output.trim_end()
                    )?;
                }
                writeln!(f)?;
            }
        }
        Ok(())
    }
}

impl TestingReport {
    pub fn normalize_execution_cpu_time(
        &self,
        execution_cpu_time: time::Duration,
    ) -> time::Duration {
        if execution_cpu_time <= self.extra_info.solution_start_time_compensation {
            time::Duration::new(0, 0)
        } else {
            (execution_cpu_time - self.extra_info.solution_start_time_compensation)
                .mul_f64(f64::from(self.extra_info.time_factor).recip())
        }
    }

    pub fn print_in_legacy_format(&self) -> anyhow::Result<()> {
        legacy::print_legacy_testing_report(self)
    }

    pub fn print_in_json_format(&self) -> anyhow::Result<()> {
        println!("{}", serde_json::to_string(self)?);
        Ok(())
    }

    pub fn print_in_human_format(&self) -> anyhow::Result<()> {
        println!("{}", self);
        Ok(())
    }
}
