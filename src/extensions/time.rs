use std::fmt;
use std::time;

use serde::{de, Deserializer, Serializer};

pub trait DurationExt {
    fn deserialize_from_seconds<'de, D>(deserializer: D) -> Result<time::Duration, D::Error>
    where
        D: Deserializer<'de>;

    fn serialize_to_seconds<S>(duration: &time::Duration, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer;
}

impl DurationExt for time::Duration {
    fn deserialize_from_seconds<'de, D>(deserializer: D) -> Result<time::Duration, D::Error>
    where
        D: Deserializer<'de>,
    {
        let float_seconds: f64 = deserializer.deserialize_f64(F64Visitor)?;
        let seconds = float_seconds as u64;
        let nanoseconds = (float_seconds.fract() * 1_000_000_000.0) as u32;
        Ok(time::Duration::new(seconds, nanoseconds))
    }

    fn serialize_to_seconds<S>(duration: &time::Duration, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_f64(duration.as_secs_f64())
    }
}

pub struct F64Visitor;

impl<'de> de::Visitor<'de> for F64Visitor {
    type Value = f64;

    fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.write_str("a float value")
    }

    fn visit_f64<E>(self, value: f64) -> Result<f64, E>
    where
        E: de::Error,
    {
        Ok(value)
    }
}
