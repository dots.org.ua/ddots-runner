use std::convert::TryInto;
use std::os::unix::process::{CommandExt, ExitStatusExt};
use std::{io, process, time};

use cgroups_fs::{AutomanagedCgroup, Cgroup, CgroupName, CgroupsCommandExt};
use log::debug;

use super::memory_size::MemorySize;

#[derive(Debug, Clone, Copy)]
pub enum SandboxStdioMode {
    Inherit,
    ErrorsRedirect,
}

#[derive(Debug, Clone)]
pub struct SandboxOptions {
    pub cgroup_namespace: std::path::PathBuf,
    pub cpu_time_limit: time::Duration,
    pub real_time_limit: time::Duration,
    pub user_memory_limit: MemorySize,
    pub kernel_memory_limit: MemorySize,
    pub processes_limit: u64,
    pub user_id: nix::unistd::Uid,
    pub group_id: nix::unistd::Gid,
    pub stdio: SandboxStdioMode,
}

impl Default for SandboxOptions {
    fn default() -> Self {
        let nobody_user = users::get_user_by_name("nobody").unwrap();
        Self {
            cgroup_namespace: "".into(),
            cpu_time_limit: time::Duration::from_secs(1),
            real_time_limit: time::Duration::from_secs(1),
            user_memory_limit: MemorySize::from_mebibytes(1),
            kernel_memory_limit: MemorySize::from_mebibytes(10),
            processes_limit: 100,
            user_id: nix::unistd::Uid::from_raw(nobody_user.uid()),
            group_id: nix::unistd::Gid::from_raw(nobody_user.primary_group_id()),
            stdio: SandboxStdioMode::Inherit,
        }
    }
}

pub trait SandboxedCommand {
    fn new_sandbox() -> Self;

    fn spawn_in_sandbox<I, S>(
        &mut self,
        cmd: I,
        options: &SandboxOptions,
    ) -> io::Result<SandboxStats>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<std::ffi::OsStr>;
}

impl SandboxedCommand for process::Command {
    fn new_sandbox() -> Self {
        let mut command = process::Command::new("env");
        command.env_clear();
        command
    }

    fn spawn_in_sandbox<I, S>(
        &mut self,
        cmd: I,
        options: &SandboxOptions,
    ) -> io::Result<SandboxStats>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<std::ffi::OsStr>,
    {
        let cgroup_name = CgroupName::new(options.cgroup_namespace.join(format!(
            "sandbox-{}-{:?}",
            process::id(),
            std::thread::current().id()
        )));

        let cgroup_pids = AutomanagedCgroup::init(&cgroup_name, "pids")?;
        cgroup_pids.set_value("pids.max", options.processes_limit)?;
        let cgroup_cpuacct = AutomanagedCgroup::init(&cgroup_name, "cpuacct")?;
        cgroup_cpuacct.set_value("cpuacct.usage", 0)?;
        let cgroup_memory = AutomanagedCgroup::init(&cgroup_name, "memory")?;
        cgroup_memory.set_value(
            "memory.limit_in_bytes",
            options.user_memory_limit.to_bytes(),
        )?;
        cgroup_memory.set_value(
            "memory.kmem.limit_in_bytes",
            options.kernel_memory_limit.to_bytes(),
        )?;
        let cgroup_freezer = AutomanagedCgroup::init(&cgroup_name, "freezer")?;

        let cgroups = [
            &cgroup_pids,
            &cgroup_memory,
            &cgroup_cpuacct,
            &cgroup_freezer,
        ];

        let filtered_env: std::collections::HashMap<String, String> = std::env::vars()
            .filter(|&(ref k, _)| k == "PATH" || k == "TERM")
            .collect();

        self.envs(&filtered_env)
            .oom()
            .cgroups(&cgroups)
            .limit_execution_time(
                cmd,
                options.cpu_time_limit,
                options.real_time_limit,
                options.user_id,
                options.group_id,
            )?;

        debug!("The command is going to be executed now...",);

        let start_timestamp = time::Instant::now();
        let output = self.output()?;
        let execution_real_time = start_timestamp.elapsed();

        kill_all_tasks(&cgroup_freezer, time::Duration::from_secs(1))?;

        let execution_cpu_time =
            time::Duration::from_nanos(cgroup_cpuacct.get_value("cpuacct.usage")?);
        let memory_peak =
            MemorySize::from_bytes(cgroup_memory.get_value("memory.max_usage_in_bytes")?);

        // Remove cgroups explicitly to handle possible errors
        let mut timeout = time::Duration::from_secs(1);
        let delay = time::Duration::from_micros(100);
        for cgroup in &cgroups {
            while let Err(error) = cgroup.remove() {
                std::thread::sleep(delay);
                timeout -= delay;
                if timeout <= time::Duration::from_secs(0) {
                    return Err(error);
                }
            }
        }

        Ok(SandboxStats {
            output,
            execution_cpu_time,
            execution_real_time,
            memory_peak,
        })
    }
}

#[derive(Debug)]
pub struct SandboxStats {
    pub output: process::Output,
    pub execution_cpu_time: time::Duration,
    pub execution_real_time: time::Duration,
    pub memory_peak: MemorySize,
}

trait OutOfMemoryCommandExt {
    fn oom(&mut self) -> &mut process::Command;
}

impl OutOfMemoryCommandExt for process::Command {
    fn oom(&mut self) -> &mut process::Command {
        let closure = move || {
            // It is a hint to out-of-memory (OOM) killer that we are ready
            // to sacrifice sandboxed processes.
            std::fs::write("/proc/self/oom_score_adj", "1000")
        };
        unsafe { self.pre_exec(closure) }
    }
}

enum TimerMode {
    CPU,
    Real,
}

/// https://github.com/rust-lang/libc/issues/1347
extern "C" {
    fn setitimer(
        which: nix::libc::c_int,
        new_value: *const nix::libc::itimerval,
        old_value: *mut nix::libc::itimerval,
    ) -> nix::libc::c_int;
}

fn set_timer(time: time::Duration, mode: TimerMode) -> io::Result<()> {
    let tval = nix::libc::itimerval {
        it_interval: nix::libc::timeval {
            tv_sec: 0,
            tv_usec: 0,
        },
        it_value: nix::libc::timeval {
            tv_sec: time.as_secs().try_into().map_err(|_| {
                io::Error::new(
                    io::ErrorKind::InvalidInput,
                    format!("Timer cannot be set for {:?}", time),
                )
            })?,
            tv_usec: time.subsec_micros().into(),
        },
    };
    let which = match mode {
        TimerMode::CPU => nix::libc::ITIMER_VIRTUAL,
        TimerMode::Real => nix::libc::ITIMER_REAL,
    };
    if unsafe { setitimer(which, &tval as *const _, std::ptr::null_mut()) } == -1 {
        Err(io::Error::last_os_error())
    } else {
        Ok(())
    }
}

trait TimelimitCommandExt {
    fn limit_execution_time<I, S>(
        &mut self,
        cmd: I,
        cpu_time_limit: time::Duration,
        real_time_limit: time::Duration,
        user_id: nix::unistd::Uid,
        group_id: nix::unistd::Gid,
    ) -> io::Result<&mut process::Command>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<std::ffi::OsStr>;
}

impl TimelimitCommandExt for process::Command {
    /// WARNING: It is an awful hack which avoids manual forking/exec'ing.
    /// We hijack the `do_exec` implementation:
    /// https://github.com/rust-lang/rust/blob/9ebf47851a357faa4cd97f4b1dc7835f6376e639/src/libstd/sys/unix/process/process_unix.rs#L239
    /// Thus, `pre_exec` will never give the control back!
    fn limit_execution_time<I, S>(
        &mut self,
        cmd: I,
        cpu_time_limit: time::Duration,
        real_time_limit: time::Duration,
        user_id: nix::unistd::Uid,
        group_id: nix::unistd::Gid,
    ) -> io::Result<&mut process::Command>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<std::ffi::OsStr>,
    {
        let mut cmd = cmd.into_iter().map(|x| x.as_ref().into());
        let executable = cmd.next().ok_or_else(|| {
            io::Error::new(
                io::ErrorKind::InvalidInput,
                "CMD must contain at least one item (executable path)",
            )
        })?;
        let args: Vec<std::ffi::OsString> = cmd.collect();
        debug!(
            "The time limited command is {:?} with args {:?}",
            executable, args
        );

        let exec = move || -> io::Result<i32> {
            let mut signals_catcher = nix::sys::signal::SigSet::empty();
            // We want to catch the child process exit event,
            signals_catcher.add(nix::sys::signal::Signal::SIGCHLD);
            // and wall-clock timeout event.
            signals_catcher.add(nix::sys::signal::Signal::SIGALRM);
            nix::sys::signal::sigprocmask(
                nix::sys::signal::SigmaskHow::SIG_SETMASK,
                Some(&signals_catcher),
                None,
            )
            .map_err(|why| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!("Sandbox could not configure the signals: {}", why),
                )
            })?;

            // We close all the inherit file descriptors to avoid dead locks.
            //
            // WARNING: Returning from this function beyond this loop will cause panic!
            // Thus, `?` and `return` must be replaced with `_exit`.
            for fd in std::fs::read_dir("/proc/self/fd/")?
                .filter_map(Result::ok)
                .filter_map(|fd_entry| fd_entry.file_name().to_str().and_then(|fd| fd.parse().ok()))
                .filter(|&fd| fd > 2)
            {
                if let Err(why) = nix::unistd::close(fd) {
                    eprintln!("Sandbox cannot close file descriptors in the fork: {}", why);
                    unsafe {
                        nix::libc::_exit(255);
                    }
                }
            }

            let mut sandboxed_command = process::Command::new(&executable);
            sandboxed_command
                .args(&args)
                .uid(user_id.as_raw())
                .gid(group_id.as_raw());

            let set_cpu_timelimit = move || {
                // Configure CPU-time timeout for the child process.
                //
                // NOTE: The child process can override the SIGALRM and ignore this timer (e.g.
                // Ruby interpreter does this by default). There is nothing we can do about it,
                // so we setup the wall-clock timeout on the parent process and send SIGKILL.
                set_timer(cpu_time_limit, TimerMode::CPU).map_err(|why| {
                    io::Error::new(
                        io::ErrorKind::Other,
                        format!("CPU timer could not be set: {}", why),
                    )
                })
            };
            unsafe {
                sandboxed_command.pre_exec(set_cpu_timelimit);
            }

            let mut sandboxed_child = match sandboxed_command.spawn() {
                Ok(child) => child,
                Err(why) => {
                    eprintln!("The sandboxed process could not be executed: {}", why);
                    unsafe {
                        nix::libc::_exit(255);
                    }
                }
            };

            if let Err(why) = set_timer(real_time_limit, TimerMode::Real) {
                eprintln!("Real-time timer could not be set: {}", why);
                unsafe {
                    nix::libc::_exit(255);
                }
            }

            loop {
                match signals_catcher.wait() {
                    Ok(nix::sys::signal::Signal::SIGCHLD) => {
                        break;
                    }
                    Ok(nix::sys::signal::Signal::SIGALRM) => {
                        if let Err(why) = sandboxed_child.kill() {
                            eprintln!("The sandboxed process could not be killed: {}", why);
                        }
                        break;
                    }
                    _ => {}
                }
            }

            let status = sandboxed_child.wait();
            Ok(status
                .ok()
                .and_then(|status| {
                    status
                        .code()
                        .map(|exit_code| if exit_code <= 128 { exit_code } else { 128 })
                        .or_else(|| status.signal().map(|exit_code| exit_code + 128))
                })
                .unwrap_or(255))
        };
        unsafe {
            self.pre_exec(move || {
                nix::libc::_exit(exec()?);
            });
        }
        Ok(self)
    }
}

/// Kills (SIGKILL) all the tasks attached to the cgroup.
fn kill_all_tasks(freezer: &Cgroup, mut timeout: time::Duration) -> io::Result<()> {
    if freezer.get_tasks()?.is_empty() {
        return Ok(());
    }

    let delay = time::Duration::from_micros(100);

    let freeze_state = freezer.set_raw_value("freezer.state", "FROZEN");
    if freeze_state.is_ok() {
        while timeout > time::Duration::from_micros(0) {
            if freezer.get_raw_value("freezer.state")?.trim() == "FROZEN" {
                break;
            }
            std::thread::sleep(delay);
            timeout -= delay;
        }
        freezer
            .send_signal_to_all_tasks(nix::sys::signal::Signal::SIGKILL)
            .ok();
    }
    freezer.set_raw_value("freezer.state", "THAWED")?;
    if freeze_state.is_err() {
        return freeze_state;
    }

    while timeout > time::Duration::from_micros(0) {
        std::thread::sleep(delay);
        timeout -= delay;
        if freezer.get_tasks()?.is_empty() {
            return Ok(());
        }
    }

    Err(io::Error::new(
        io::ErrorKind::Other,
        "child subprocess(es) survived SIGKILL",
    ))
}
