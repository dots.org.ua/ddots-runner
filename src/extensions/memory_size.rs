use std::cmp::Ordering;
use std::fmt;
use std::ops::Mul;

use serde::{de, Deserializer, Serializer};

#[derive(Debug, Default, Eq, Copy, Clone)]
pub struct MemorySize {
    size_in_bytes: u64,
}

impl MemorySize {
    pub fn from_bytes(size_in_bytes: u64) -> Self {
        Self { size_in_bytes }
    }

    pub fn from_kibibytes(size_in_kibibytes: u64) -> Self {
        Self::from_bytes(size_in_kibibytes * 1024)
    }

    pub fn from_mebibytes(size_in_mebibytes: u64) -> Self {
        Self::from_kibibytes(size_in_mebibytes * 1024)
    }

    pub fn deserialize_from_mebibytes<'de, D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Self::from_mebibytes(
            deserializer.deserialize_u64(U64Visitor)?,
        ))
    }

    pub fn to_bytes(self) -> u64 {
        self.size_in_bytes
    }

    pub fn to_kibibytes(self) -> u64 {
        self.to_bytes() / 1024
    }

    pub fn to_mebibytes(self) -> u64 {
        self.to_kibibytes() / 1024
    }

    #[allow(clippy::trivially_copy_pass_by_ref)]
    pub fn serialize_to_bytes<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_u64(self.to_bytes())
    }
}

impl Ord for MemorySize {
    fn cmp(&self, other: &Self) -> Ordering {
        self.size_in_bytes.cmp(&other.size_in_bytes)
    }
}

impl PartialOrd for MemorySize {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.size_in_bytes.partial_cmp(&other.size_in_bytes)
    }
}

impl PartialEq for MemorySize {
    fn eq(&self, other: &Self) -> bool {
        self.size_in_bytes == other.size_in_bytes
    }
}

impl Mul<f32> for MemorySize {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        Self::Output::from_bytes((self.size_in_bytes as f32 * rhs) as u64)
    }
}

struct U64Visitor;

impl<'de> de::Visitor<'de> for U64Visitor {
    type Value = u64;

    fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.write_str("an integer between 0 and 2^64")
    }

    fn visit_u64<E>(self, value: u64) -> Result<u64, E>
    where
        E: de::Error,
    {
        Ok(value)
    }
}
