#![allow(unstable_name_collisions)]

pub mod checker;
pub mod common;
pub mod extensions;
pub mod generator;
pub mod solution_testing;

pub use self::common::{Config, ProblemSpecification};
pub use self::solution_testing::{test_solution, TestingContext};
