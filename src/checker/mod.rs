use std::convert::TryFrom;
use std::fs;
use std::path::PathBuf;
use std::process::Command;

use anyhow::{bail, Context};
use log::{debug, error, info, warn};
use nix::sys::signal::Signal;

use crate::common::SolutionTestStatus;
use crate::extensions::process::{SandboxOptions, SandboxedCommand};

pub struct CheckerReport {
    pub status: SolutionTestStatus,
    pub message: String,
}

#[derive(Debug)]
pub struct Checker<'a> {
    sandbox_options: &'a SandboxOptions,
    sandbox_root: &'a PathBuf,
    checker_exe_filepath: &'a PathBuf,
    test_input_filepath: &'a PathBuf,
    test_answer_filepath: &'a PathBuf,
    solution_output_filepath: &'a PathBuf,
    solution_process_status_code: Option<i32>,
}

impl<'a> Checker<'a> {
    pub fn new(
        sandbox_options: &'a SandboxOptions,
        sandbox_root: &'a PathBuf,
        checker_exe_filepath: &'a PathBuf,
        test_input_filepath: &'a PathBuf,
        test_answer_filepath: &'a PathBuf,
        solution_output_filepath: &'a PathBuf,
        solution_process_status_code: Option<i32>,
    ) -> Self {
        Self {
            sandbox_options,
            sandbox_root,
            checker_exe_filepath,
            test_input_filepath,
            test_answer_filepath,
            solution_output_filepath,
            solution_process_status_code,
        }
    }

    pub fn execute(&self) -> anyhow::Result<CheckerReport> {
        let checker_process = Command::new_sandbox()
            .current_dir(&self.sandbox_root)
            .spawn_in_sandbox(
                &[
                    self.checker_exe_filepath,
                    self.test_input_filepath,
                    self.solution_output_filepath,
                    self.test_answer_filepath,
                ],
                self.sandbox_options,
            )
            .context("The checker could not be executed.")?;

        let checker_exit_code = checker_process.output.status.code();
        debug!(
            "The checker exited with {:?} exit code and produced the following stderr:\n{}",
            checker_exit_code,
            String::from_utf8_lossy(&checker_process.output.stderr)
        );

        let status = match checker_exit_code {
            Some(0) => {
                info!("The checker reported OK.");
                SolutionTestStatus::OK
            }
            Some(1) => {
                info!("The checker reported WA.");
                // Since stdout file is created immediately on solution execution (due
                // to the output redirect), and some checkers treat empty output as WA
                // rather than PE, Testing System has to handle such situations on its
                // own.
                let solution_output_size = fs::metadata(&self.solution_output_filepath)
                    .map(|solution_output_file_metadata| solution_output_file_metadata.len())
                    .unwrap_or(0);
                if solution_output_size > 0 {
                    SolutionTestStatus::WrongAnswer
                } else {
                    info!(
                        "Even though the checker reported WA, we report it as PE \
                         or RE due to the fact that the solution output is empty."
                    );
                    // If the output is empty and exit code is not zero, it is highly
                    // probable that the solution has just crashed. For example, C#,
                    // Java, Python return exit code 1 on runtime errors.
                    match self.solution_process_status_code {
                        Some(0) => SolutionTestStatus::PresentationError,
                        _ => SolutionTestStatus::RuntimeError,
                    }
                }
            }
            Some(2) => {
                info!("The checker reported PE.");
                // Sometimes application may just crash while writing to the output
                // file, so it is not a PE, but rather RE. For example, Mono (C#)
                // returns exit code 1 on unhandled exceptions, which may happen due to
                // the memory limit restrictions.
                match self.solution_process_status_code {
                    Some(0) => SolutionTestStatus::PresentationError,
                    _ => {
                        info!(
                            "Even though the checker reported PE, we report it \
                             as RE because the solution exit code was non-zero."
                        );
                        SolutionTestStatus::RuntimeError
                    }
                }
            }
            Some(3) => {
                info!(
                    "The checker reported a failure. (stdout: \"{:?}\", stderr: \"{:?}\")",
                    checker_process.output.stdout, checker_process.output.stderr
                );
                SolutionTestStatus::CheckerCrash
            }
            Some(127) => {
                warn!(
                    "The checker could not be started. (stdout: \"{:?}\", stderr: \"{:?}\")",
                    checker_process.output.stdout, checker_process.output.stderr
                );
                // "command not found" or missing shared libraries error
                SolutionTestStatus::CheckerCrash
            }
            Some(255) => {
                error!(
                    "The checker failed due to an internal error: {}",
                    String::from_utf8_lossy(&checker_process.output.stderr)
                );
                SolutionTestStatus::UnexpectedError
            }
            Some(checker_exit_code) => {
                match Signal::try_from(checker_exit_code - 128) {
                    // Checker misbehaved and SIGKILL signal was sent to it due to ML or TL.
                    Ok(signal_name @ Signal::SIGKILL)
                    // Checker crashed due to a Runtime Error.
                    | Ok(signal_name @ Signal::SIGSEGV)
                    // Checker misbehaved and SIGVTALRM signal was sent to it due to TL.
                    | Ok(signal_name @ Signal::SIGVTALRM) => {
                        info!(
                            "The checker has been interrupted due to ML, TL, or \
                            RE reasons: {:?}",
                            signal_name
                        );
                        SolutionTestStatus::CheckerCrash
                    },
                    _ => {
                        info!(
                            "The checker exited with an unexpected exit code: {}",
                            checker_exit_code
                        );
                        SolutionTestStatus::UnexpectedError
                    },
                }
            }

            None => bail!("The exit code of checker execution is not set."),
        };

        let stdout = String::from_utf8_lossy(&checker_process.output.stdout);
        let stderr = String::from_utf8_lossy(&checker_process.output.stderr);
        let message = if stdout.is_empty() {
            stderr.into()
        } else if stderr.is_empty() {
            stdout.into()
        } else {
            format!("{}\n{}", stdout, stderr)
        };

        Ok(CheckerReport { status, message })
    }
}
