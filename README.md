DDOTS Runner
============

DDOTS Runner manages the testing process of a single (compiled) solution. It expects to receive a
problem specification (a folder with `Problem.xml`) and a compiled (if applicable) solution, and
produces a testing report.

NOTE: It heavily relies on a modern Linux kernel (4.4+,
[but avoid 4.6-4.13](https://bugzilla.kernel.org/show_bug.cgi?id=190841)), namely CGroup subsystem
to account and limit memory and CPU usage.

Build
-----

### Simple local build

You will need to have a stable Rust (1.32+) compiler installed.

```
$ cargo build --release
```

### Build statically in a Docker container

You don't need to have a Rust compiler installed on your computer, but you have
to have Docker.

```
$ docker run --rm -v "$(pwd):/home/rust/src" ekidd/rust-musl-builder:stable cargo build --release
```

The above command will produce the binary file `target/x86_64-unknown-linux-musl/release/ddots-runner`.

Sometimes, you want to share Cargo registry between builds to speed up the
compilation process, so you can use the following command:

```
$ docker run --rm -v "$(pwd):/home/rust/src" -v "$HOME/.cargo/registry:/home/rust/.cargo/registry" ekidd/rust-musl-builder:stable cargo build --release
```

NOTE: DDOTS Runner Environments (a.k.a ddots-runners) handle the build process automatically.


Run
---

NOTE: The problem folder has to be accessible by the "semitrusted service"
sandbox user (by default, `daemon`), so for the sake of the example, we copy
the problem folder to `/tmp/`:

```
$ cp -r ./tests/fixtures/problems_db/1001/ /tmp/
```

NOTE: `ddots-runner` requires root permissions to set up CGroups, but the
privileges are dropped when the solution gets executed. `solution-sandbox-user`
(by default, `nobody`) and `semitrusted-service-sandbox-user` (by default,
`daemon`) are used when the external processes are executed.

We are ready to test the solution:

```
$ sudo ./target/release/ddots-runner --problem-root /tmp/1001/ --solution-filepath /tmp/1001/authors/bash.sh
```

You should see the testing report:

```
Extra info:
  Global time factor: 1.00
  Solution start time compensation: 0.000s

Tests:
Test #001 (OK) took 0.002s normalized CPU time / 0.002s true CPU time / 0.002s real time and peaked at 0.852 MiB.
  Scored points: 1
```

Learn other CLI arguments by running `ddots-runner --help`.
