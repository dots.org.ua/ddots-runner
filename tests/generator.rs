use ddots_runner::common::SolutionTestStatus;

mod helpers;
use helpers::check_solution;

type TestResult = Result<(), failure::Error>;

const GENERATOR_PROBLEM_PATH: &str = "./tests/fixtures/problems_db/generator-example";

#[test]
fn generator_with_author_solution_should_pass() -> TestResult {
    let report = check_solution(GENERATOR_PROBLEM_PATH, "solutions/ok.sh")?;
    assert!(
        report.tests.iter().all(|test| test.is_ok()),
        "author solution expected to pass, but: {:?}",
        report
    );
    Ok(())
}

#[test]
fn generator_with_wrong_solution_should_report_wrong_answer() -> TestResult {
    let report = check_solution(GENERATOR_PROBLEM_PATH, "solutions/wrong-answer.sh")?;
    assert!(
        report.tests.iter().all(|test| match test.status {
            Some(SolutionTestStatus::WrongAnswer) => true,
            _ => false,
        }),
        "wrong solution expected to report WA, but: {:?}",
        report
    );
    Ok(())
}
