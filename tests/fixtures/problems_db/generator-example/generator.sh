#!/bin/sh
pwd >&2

echo "1" >&2
SHARED_GENERATOR_CONTEXT_FILENAME="$1"
shared_generator_context="$(cat "$SHARED_GENERATOR_CONTEXT_FILENAME")"
previous_solution_output="$(cat "$2")"
echo "2" >&2

if [ -z "$previous_solution_output" ]; then
    echo "3" >&2
    echo 1
    exit 0
fi

if [ "$previous_solution_output" -lt 15 ]; then
    echo "$(( shared_generator_context + 3 ))" > "$SHARED_GENERATOR_CONTEXT_FILENAME"
else
    echo "$(( shared_generator_context - 50 ))" > "$SHARED_GENERATOR_CONTEXT_FILENAME"
fi

echo "$(( shared_generator_context + previous_solution_output ))"
