#!/bin/sh

set -x -e
pwd >&2
ls -lah >&2
ls -lah "$1" "$2" >&2

read 'input' <"$1"
read 'output' <"$2"
if [ "$output" -eq "$(( input + 1 ))" ]; then
    exit 0
fi

exit 1
