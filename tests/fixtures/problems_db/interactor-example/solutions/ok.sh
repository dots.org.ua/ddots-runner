#!/bin/sh

left=0
right=1000

while [ "$left" -lt "$right" ]; do
    mid=$(( (left + right) / 2))
    echo "$mid"
    read 'response'
    if [ "$response" = '-1' ]; then
        right="$mid"
    else
        if [ "$response" = '1' ]; then
            left="$mid"
        else
            exit 0
        fi
    fi
done
