#include <stdio.h>

int main() {
    int left = 0;
    int right = 1000;
    int hint;
    while (left < right) {
        int mid = (left + right) / 2;
        printf("%d\n", mid);
        scanf("%d", &hint);
        if (hint == 0) {
            return 0;
        }
        if (hint == -1) {
            right = mid;
        } else {
            left = mid;
        }
    }
}
