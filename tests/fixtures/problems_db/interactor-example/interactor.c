#include <stdio.h>

int main() {
    const int hidden_number = 7;
    int number;
    for (int i = 0; i < 10; ++i) {
        if (scanf("%d", &number) != 1) {
            return 2;
        }
        if (hidden_number == number) {
            puts("0");
            return 0;
        }
        if (hidden_number < number) {
            puts("-1");
        } else {
            puts("1");
        }
    }
    return 1;
}
