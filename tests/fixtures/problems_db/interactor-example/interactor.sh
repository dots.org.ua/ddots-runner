#!/bin/sh

E_OK=0
E_WRONG_ANSWER=1
E_PRESENTATION_ERROR=2

read 'hidden_number' <answer.txt

attempts=0
while [ "$attempts" -lt 10 ]; do
    read 'x' || exit "$E_PRESENTATION_ERROR"  # read failed
    [ "$x" -eq "$x" ] 2>/dev/null || exit "$E_PRESENTATION_ERROR"  # the read value is not a number
    if [ "$x" -eq "$hidden_number" ]; then
        echo '0'
        exit "$E_OK"
    fi
    if [ "$x" -gt "$hidden_number" ]; then
        echo '-1'
    else
        echo '1'
    fi
    attempts=$((attempts + 1))
done

exit "$E_WRONG_ANSWER"
