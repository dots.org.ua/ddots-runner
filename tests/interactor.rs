use failure;

use ddots_runner::common::SolutionTestStatus;

mod helpers;
use helpers::check_solution;

const INTERACTOR_PROBLEM_PATH: &str = "./tests/fixtures/problems_db/interactor-example";

#[test]
fn interactor_with_author_solution_should_pass() -> Result<(), failure::Error> {
    let report = check_solution(INTERACTOR_PROBLEM_PATH, "solutions/ok.sh")?;
    assert!(
        report.tests.iter().all(|test| test.is_ok()),
        "author solution expected to pass, but: {:?}",
        report
    );
    Ok(())
}

#[test]
fn interactor_with_hang_solution_should_report_forbidden_function() -> Result<(), failure::Error> {
    let report = check_solution(INTERACTOR_PROBLEM_PATH, "solutions/hang.sh")?;
    assert!(
        report.tests.iter().all(|test| {
            match test.status {
                Some(SolutionTestStatus::ForbiddenFunctionDetected) => true,
                _ => false,
            }
        }),
        "hanging solution hadn't been caught: {:?}",
        report
    );
    Ok(())
}

#[test]
fn interactor_with_incorrect_solution_should_report_wrong_answer() -> Result<(), failure::Error> {
    let report = check_solution(INTERACTOR_PROBLEM_PATH, "solutions/wrong-answer.sh")?;
    assert!(
        report.tests.iter().all(|test| {
            match test.status {
                Some(SolutionTestStatus::WrongAnswer) => true,
                _ => false,
            }
        }),
        "wrong answer solution hadn't been caught: {:?}",
        report
    );
    Ok(())
}

#[test]
fn interactor_with_broken_solution_should_report_presentation_error() -> Result<(), failure::Error>
{
    let report = check_solution(INTERACTOR_PROBLEM_PATH, "solutions/presentation-error.sh")?;
    assert!(
        report.tests.iter().all(|test| {
            match test.status {
                Some(SolutionTestStatus::PresentationError) => true,
                _ => false,
            }
        }),
        "presentation error solution hadn't been caught: {:?}",
        report
    );
    Ok(())
}
