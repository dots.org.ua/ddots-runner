use std::path::{Path, PathBuf};

use failure::Error;
use structopt::StructOpt;

use ddots_runner::{common::TestingReport, Config, TestingContext};

pub fn setup_sandbox() -> Result<(PathBuf, tempfile::TempDir), Error> {
    let _ = env_logger::builder().is_test(true).try_init();
    let tmp = tempfile::tempdir()?;
    let sandbox_root = tmp.path().join("sandbox");
    std::fs::create_dir(&sandbox_root)?;
    Ok((sandbox_root, tmp))
}

pub fn check_solution<P1, P2>(problem_path: P1, solution_path: P2) -> Result<TestingReport, Error>
where
    P1: AsRef<Path>,
    P2: AsRef<Path>,
{
    std::env::set_var(
        "PATH",
        format!(
            "{}:{}",
            std::env::var("PATH").unwrap(),
            std::env::current_dir()
                .unwrap()
                .join("..")
                .join("timelimit")
                .to_str()
                .unwrap()
        ),
    );

    let (sandbox_root, _tmp) = setup_sandbox()?;
    let problem_root = std::fs::canonicalize(problem_path)?;
    let solution_filepath = problem_root.join(solution_path);

    let config = Config::from_iter_safe(&[
        "tests",
        "--sandbox-root",
        sandbox_root.to_str().unwrap(),
        "--problem-root",
        problem_root.to_str().unwrap(),
        "--solution-filepath",
        solution_filepath.to_str().unwrap(),
        "--semitrusted-service-sandbox-user",
        "root",
        "--sandbox-cgroup-namespace",
        "",
    ])?;

    let mut testing_context = TestingContext::from_config(&config)?;
    Ok(ddots_runner::test_solution(&mut testing_context)?)
}
