FROM ekidd/rust-musl-builder:stable

COPY ./ ./
RUN cargo test --release && \
    cargo build --release && \
    mv ./target/x86_64-unknown-linux-musl/release/ddots-runner /tmp/ && \
    rm -rf ./target ~/.cargo/registry ~/.cargo/git

FROM scratch

COPY --from=0 /tmp/ddots-runner /
